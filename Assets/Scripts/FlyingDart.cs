﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingDart : MonoBehaviour
{
    public Vector2 boundaryFromOrigin { get; set; }

    private Rigidbody2D rigidBody;
    private SpriteRenderer spriteRenderer;

    public void setColour(Color colour)
    {
        spriteRenderer.color = colour;
    }
    public void setVelocity(Vector2 velocity)
    {
        rigidBody.velocity = velocity;

        rigidBody.transform.rotation = Quaternion.LookRotation(Vector3.forward, rigidBody.velocity);

    }

    // Start is called before the first frame update
    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        rigidBody = GetComponent<Rigidbody2D>();
    }
     
    // FixedUpdate is called in sync with the physics engine.
    private void FixedUpdate()
    {
        rigidBody.transform.rotation = Quaternion.LookRotation(Vector3.forward, rigidBody.velocity);

        Vector2 pos = rigidBody.position;
        if(pos.x > boundaryFromOrigin.x || pos.x < -boundaryFromOrigin.x)
        {
            pos.x = -pos.x;
        }
        if (pos.y > boundaryFromOrigin.y || pos.y < -boundaryFromOrigin.y)
        {
            pos.y = -pos.y;
        }
        rigidBody.transform.position = pos;

    }

    private void OnDrawGizmos()
    {
        Color rayColour = new Color(0.5f, 0.5f, 0.5f, 0.5f);
        Gizmos.color = rayColour;
        Vector3 pos = transform.position;
        for (int i = 0; i < DartViewRays.rayCount; i++)
        {
            Vector3 rayDirection = rigidBody.transform.TransformDirection(DartViewRays.rayDirections[i]);
            Gizmos.color = DartViewRays.rayColours[i];
            //Gizmos.DrawLine(pos, pos + rayDirection);
            RaycastHit2D rayCastHit = Physics2D.CircleCast(pos, DartViewRays.cirleCastRadius, rayDirection, DartViewRays.castDistance, DartViewRays.collisionLayerMask);
            if(rayCastHit.collider != null)
            {

                Gizmos.color = spriteRenderer.color;
                Gizmos.DrawLine(pos, rayCastHit.point);
                Gizmos.color = rayColour;
            }
        }
        Gizmos.color = new Color(0.5f, 1.0f, 0.5f, 1.0f);
        Vector3 clearDirection = collisionFreeDirection();
        Gizmos.DrawLine(pos, pos + clearDirection);
    }

    Vector3 collisionFreeDirection()
    {
        Vector2 pos = rigidBody.transform.position;
        for (int i = 0; i < DartViewRays.rayCount; i++)
        {
            Vector3 direction = rigidBody.transform.TransformDirection(DartViewRays.rayDirections[i]); ;

            if (Physics2D.CircleCast(pos, DartViewRays.cirleCastRadius, direction, DartViewRays.castDistance, DartViewRays.collisionLayerMask).collider == null)
            {
                return direction;
            }
        }
        return rigidBody.transform.forward;
    }

    // Update is called once per frame
    void Update()
    {
    }
}
