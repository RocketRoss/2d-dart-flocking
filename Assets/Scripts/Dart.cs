﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Dart : MonoBehaviour
{
    private Vector2 velocity = new Vector2(2f, 3f);

    Dart()
    {
        velocity = new Vector2(2f, 3f);
    }

    // Start is called before the first frame update
    void Start()
    {
        // Create Vector2 vertices
        Vector2[] vertices2D  = new Vector2[] {
            new Vector2(0,1),
            new Vector2(3,0),
            new Vector2(0,-1),
        };

        // Use the triangulator to get indices for creating triangles
        Triangulator tr = new Triangulator(vertices2D);
        int[] indices = tr.Triangulate();

        // Create the Vector3 vertices
        Vector3[] vertices = new Vector3[vertices2D.Length];
        // create new colors array where the colors will be created.
        Color[] colours = new Color[vertices2D.Length];
        for (int i = 0; i < vertices.Length; i++)
        {
            colours[i] = Color.Lerp(Color.red, Color.blue, ((float)i)/vertices.Length);
            vertices[i] = new Vector3(vertices2D[i].x, vertices2D[i].y, 0);
        }

        // Create the mesh
        var mesh = new Mesh
        {
            vertices = vertices,
            triangles = indices,
            colors = colours
        };
        mesh.RecalculateNormals();
        mesh.RecalculateBounds();

        // Set up game object with mesh - without this, the colours do not get rendered.
        var meshRenderer = gameObject.AddComponent<MeshRenderer>();
        meshRenderer.material = new Material(Shader.Find("Sprites/Default"));

        // Set up game object with mesh;
        MeshFilter filter = gameObject.AddComponent(typeof(MeshFilter)) as MeshFilter;
        filter.mesh = mesh;
    }

    void move()
    {
        transform.Translate(Time.deltaTime*velocity.x, Time.deltaTime * velocity.y, 0);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
