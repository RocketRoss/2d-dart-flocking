﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class DartSettings : ScriptableObject
{
    // Settings
    [Header("Motion")]
    public float minSpeed = 2;
    public float maxSpeed = 5;
    public float perceptionRadius = 2.5f;
    public float avoidanceRadius = 1;
    public float maxSteerForce = 3;

    [Header("Collision Ray Casting")]
    public LayerMask obstacleMask;

    public float CircleRadius;
    public float RayCastDistance;

}