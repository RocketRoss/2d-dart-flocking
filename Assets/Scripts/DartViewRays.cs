﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DartViewRays
{
    public const int rayCount = 300;
    const float fieldOfView = 110*Mathf.PI/180;

    public const float cirleCastRadius = 0.2f;
    public const float castDistance = 2.0f;
    public const int collisionLayerMask = (1 << 8);

    public static readonly Vector3[] rayDirections;
    public static readonly Color[] rayColours;
    static DartViewRays()
    {
        rayDirections = new Vector3[rayCount];
        rayColours = new Color[rayCount];
        int halfRayCount = rayCount / 2;
        for (int i = 0; i < halfRayCount; i++)
        {
            float progress = (float)i / halfRayCount;
            rayColours[i] = new Color(progress, progress, progress);
            rayColours[i + halfRayCount] = new Color(progress, progress, progress);

            float azimuth = (fieldOfView * progress);
            float x = Mathf.Sin(azimuth);
            float y = Mathf.Cos(azimuth);
            rayDirections[i] = new Vector3(x, y);
            x = Mathf.Sin(-azimuth);
            y = Mathf.Cos(-azimuth);
            rayDirections[i + halfRayCount] = new Vector3(x, y);
        }
    }
}
