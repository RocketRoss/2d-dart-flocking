﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DartSpawner : MonoBehaviour
{

    public FlyingDart prefab;
    [Header("Space")]
    public Vector2 boundaryFromOrigin;
    public float spawnRadius = 10;
    public int spawnCount = 10;

    [Header("Colour")]
    public Color colour;
    public bool randomiseColour;

    private void Awake()
    {
        Vector2 spawnerLocation = new Vector2(transform.position.x, transform.position.y);
        Vector2 boundaryFromOriginScaled = boundaryFromOrigin;
        boundaryFromOriginScaled.Scale(new Vector2(0.5f, 0.5f));
        for (int i = 0; i < spawnCount; i++)
        {
            FlyingDart dart = Instantiate<FlyingDart>(prefab, transform);
            dart.transform.position = spawnerLocation + Random.insideUnitCircle * spawnRadius;
            Vector3 velo = Random.onUnitSphere;
            dart.setVelocity(new Vector2(velo.x, velo.y).normalized);
            if (randomiseColour)
            {
                dart.setColour(Random.ColorHSV());
            }
            else
            {
                dart.setColour(new Color(colour.r, colour.g, colour.b));
            }
            dart.boundaryFromOrigin = boundaryFromOriginScaled;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.white;
        Gizmos.DrawWireCube(transform.position, new Vector3(boundaryFromOrigin.x, boundaryFromOrigin.y, 0));
    }
}
